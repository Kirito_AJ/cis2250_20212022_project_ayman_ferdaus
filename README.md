# About #

This repository will contain all the information about building the CIS2250 Tutor Administration Project. By using a web application this project allows students to be matched with
their respective tutors and the administration of payments. My team consists of only one member (Myself).
Teal is the color of our team project.

### Team Member ###
AJ - fayman@hollandcollege.com  

### Contact ###

BJ MacLean (Learning Manager) - CIS2250 Mobile Application Development.  
bjmaclean@hollandcollege.com  


